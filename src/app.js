/**
 * Overwatch-Monitoring
 * Monitoring bot for website / ips/ ports.
 * Author: Roguyt
 */

/**
 * Modules dependencies
 */

const CommandController = require('./controllers').Command;
const NotifyController = require('./controllers').Notify;

/**
 * Additional dependencies
 */

const config = require('./configs/config');

/**
 * Configure Winston Logger
 */

const logger = require('./configs/logger');

logger.debug('Launching Winston Logger');

/**
 * TODO
 */

let platform;

switch(config.platform) {
    case 'telegram':
        platform = require('./controllers/telegram');
        break;
    case 'slack':
        platform = require('./controllers/slack');
        break;
}

CommandController(platform);
NotifyController(platform);

/**
 * Tasks
 */

require('./tasks/updateHostsStatus');

