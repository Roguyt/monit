/**
 * Modules dependencies
 */

const fs = require('fs');
const path = require('path');

const pkg = require('../../package.json');

/**
 * Configuration file
 */

const config                  = {};

config.env                    = process.env.NODE_ENV || 'production';

config.version                = pkg.version;

/**
 * Schedule
 */

config.schedule         = {};
config.schedule.delay   = 120000;
config.schedule.timeout = 5;
config.schedule.retry   = 5;

/**
 * Redis
 */

config.redis      = {};
config.redis.name = "overwatch-monitoring";

/**
 * Platform
 */

config.platform = "" // telegram / slack

/**
 * Slack API
 */

config.slack            = {};
config.slack.token      = "REPLACE_ME";
config.slack.channel    = "REPLACE_ME";
config.slack.bot        = {};
config.slack.bot.name   = "REPLACE_ME";
config.slack.bot.avatar = "REPLACE_ME";

/**
 * Telegram API
 */

config.telegram         = {};
config.telegram.token   = "REPLACE_ME";
config.telegram.channel = "REPLACE_ME";

/**
 * Templates
 */

config.templates      = {};
config.templates.help = `Voici votre identifiant de canal: {{ channelId }}\nVersion: {{ version }}`;
config.templates.status = `{{ emojis.report }}{{ emojis.report }}{{ emojis.report }} Report {{ emojis.report }}{{ emojis.report }}{{ emojis.report }}\n{{ status }}\nLast update ({{ date }})`;
config.templates.isUp = `{{ emojis.online }} {{ name }}({{ host }})({{ elapsedTime }}ms) is UP\n`;
config.templates.isDown = `{{ emojis.offline }} {{ name }}({{ host }}) is DOWN\n`;
config.templates.wentDown = `{{ emojis.offline }}{{ emojis.offline }}{{ emojis.offline }} {{ name }}({{ host }}) is DOWN {{ emojis.offline }}{{ emojis.offline }}{{ emojis.offline }}`;
config.templates.wentUp = `{{ emojis.online }}{{ emojis.online }}{{ emojis.online }} {{ name }}({{ host }}) is back UP (was DOWN for {{ downTime }}min) {{ emojis.online }}{{ emojis.online }}{{ emojis.online }}`;

/**
 * Emoji
 */

config.emojis = {};
config.emojis.online = '✅';
config.emojis.offline = '🔴';
config.emojis.report = '🔵';

/**
 * Services
 */

config.services = [
    {
        host: "REPLACE_ME",
        name: "REPLACE_ME"
    }, {
        host: "REPLACE_ME",
        name: "REPLACE_ME"
    }
];

module.exports = config;