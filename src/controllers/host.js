/**
 * Modules dependencies
 */

const net = require('net');
const exec = require('child_process').exec;
const async = require('async');
const request = require('request');
const portscanner = require('portscanner');

const CacheController = require('./cache');

/**
 * Additional dependencies
 */

const config = require('../configs/config');

/**
 * Cache controller
 */

const self = module.exports = {
    updateHostsStatus: () => {
        async
            .each(config.services, (service, callback) => {
                self
                    .isUp(service)
                    .then((data) => {
                        callback(false);
                    })
                    .catch((err) => {
                        // BETTER ERROR
                    })
            }, (err) => {
                // Handle better here with logger and so
                CacheController
                    .set(config.services)
                ;
            })
        ;
    },
    isUp: (args) => {
        return new Promise((resolve, reject) => {
            // IPv4
            if (net.isIPv4(args.host.replace(/:\d{2,}/, ''))) {
                isUpIPv4(args.host)
                    .then((res) => {
                        args.status = res;

                        resolve(args);
                    })
                ;
            } else if (args.host.match(/(http[s]?:\/\/)?([^\/\s]+\/)(.*)/)) {
                isUpWebsite(args.host)
                    .then((res) => {
                        args.status = res;

                        resolve(args);
                    })
                ;
            } else {
                reject(); // TODO Good error
            }
        })
    }
};

function isUpIPv4(host) {
    return new Promise((resolve, reject) => {
        if (!host.match(/:\d{2,}/)) {
            ping(host)
                .then((data) => {
                    if (data.isUp === true) {
                        resolve(data);
                    } else {
                        let count = 0;

                        async
                            .whilst(() => {
                                if (data.isUp === true) {
                                    return false;
                                }

                                return count < config.schedule.retry;
                            }, (callback) => {
                                count++;

                                setTimeout(() => {
                                    ping(host)
                                        .then((tmp) => {
                                            data = tmp;

                                            callback(null, data);
                                        })
                                    ;
                                }, 2000);
                            }, (err, data) => {
                                resolve(data);
                            })
                        ;
                    }
                })
            ;
        } else {
            open(host)
                .then((data) => {
                    if (data.isUp === true) {
                        resolve(data)
                    } else {
                        let count = 0;

                        async
                            .whilst(() => {
                                if (data.isUp === true) {
                                    return false;
                                }

                                return count < config.schedule.retry;
                            }, (callback) => {
                                count++;

                                setTimeout(() => {
                                    open(host)
                                        .then((tmp) => {
                                            data = tmp;

                                            callback(null, data);
                                        })
                                    ;
                                }, 2000);
                            }, (err, data) => {
                                resolve(data);
                            })
                        ;
                    }
                })
            ;
        }
    });
}

function isUpWebsite(host) {
    return new Promise((resolve, reject) => {
        get(host)
            .then((data) => {
                if (data.isUp === true) {
                    resolve(data);
                } else {
                    let count = 0;

                    async
                        .whilst(() => {
                            if (data.isUp === true) {
                                return false;
                            }

                            return count < config.schedule.retry;
                        }, (callback) => {
                            count++;

                            setTimeout(() => {
                                get(host)
                                    .then((tmp) => {
                                        data = tmp;

                                        callback(null, data);
                                    })
                                ;
                            }, 2000);
                        }, (err, data) => {
                            resolve(data);
                        })
                    ;
                }
            })
        ;
    });
}

function ping(host) {
    return new Promise((resolve, reject) => {
        let command;

        if (/^win/.test(process.platform)) {
            command = 'ping -w ' + config.schedule.timeout + '000 -n 1 ';
        } else {
            command = 'ping -W ' + config.schedule.timeout + ' -c 1 ';
        }

        exec(command + '' + host, (err, stdout, stderr) => {
            if (!err && stdout.match(/=\d+(\.\d+)?\s?ms/)) {
                resolve({
                    isUp: true,
                    elapsedTime: stdout.match(/=\d+(\.\d+)?\s?ms/)[0].replace(/\s?ms/, '').replace('=', '')
                });
            } else {
                resolve({
                    isUp: false,
                    elapsedTime: -1
                });
            }
        })
    });
}

function open(host) {
    return new Promise((resolve, reject) => {
        let port = host.match(/:\d{2,}/)[0].replace(':', '');
        let ip = host.replace(target.match(/:\d{2,}/)[0], '');

        portscanner.checkPortStatus(port, ip, (err, status) => {
            if (status === 'open') {
                resolve({
                    isUp: true,
                    elapsedTime: -1
                });
            } else {
                resolve({
                    isUp: false,
                    elapsedTime: -1
                })
            }
        });
    });
}

function get(host) {
    return new Promise((resolve, reject) => {
        request({
            url: host,
            timeout: config.schedule.timeout * 1000,
            time: true
        }, (err, res, body) => {
            if (!err && res.statusCode === 200) {
                resolve({
                    isUp: true,
                    elapsedTime: res.timingPhases.total.toFixed(0)
                });
            } else {
                resolve({
                    isUp: false,
                    elapsedTime: -1
                });
            }
        })
    })
}