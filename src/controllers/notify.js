/**
 * Modules dependencies
 */

const async = require('async');
const redis = require('redis');
const bluebird = require('bluebird');

const TemplateController = require('./template');
const CacheController = require('./cache');

/**
 * Additional dependencies
 */

const config = require('../configs/config');

/**
 * Initialize Redis Client
 */

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const redisClient = redis.createClient();
const sub = redis.createClient();

sub.subscribe(config.redis.name + ':data:notify');

/**
 * Command controller
 */

const self = module.exports = (interface) => {
    let interfaceConfig = interface.getConfig();

    sub.on('message', (channel, message) => {
        if (channel === config.redis.name + ':data:notify') {
            CacheController
                .get()
                .then((data) => {
                    async
                        .each(data.datas, (host, callback) => {
                            if (!host.status.isUp && !hostDownTime[host.host]) {
                                hostDownTime[host.host] = new Date();

                                TemplateController
                                    .render('wentDown', {
                                        host: host
                                    })
                                    .then((data) => {
                                        interface.send(data, interfaceConfig.channel)
                                    })
                                ;
                            } else if (host.status.isUp && hostDownTime[host.host]) {
                                let downTime = Math.abs((new Date()).getTime() - hostDownTime[host.host]);
                                downTime = Math.ceil(downTime / (1000 * 60));

                                TemplateController
                                    .render('wentUp', {
                                        host: host,
                                        downTime: downTime
                                    })
                                    .then((data) => {
                                        interface.send(data, interfaceConfig.channel)
                                    })
                                ;

                                delete hostDownTime[host.host];
                            }
                        }, (err) => {
                            // TODO Somthing to do or not ?
                        })
                })
            ;
        }
    })
};

let hostDownTime = {};