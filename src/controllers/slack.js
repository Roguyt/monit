/**
 * Modules dependencies
 */

const slackBots = require('slackbots');

/**
 * Additional dependencies
 */

const config = require('../configs/config');

/**
 * Initialize Slack Client
 */

let client = new slackBots({
    token: config.slack.token,
    name: config.slack.bot.name
});

/**
 * Command controller
 */

const self = module.exports = {
    getConfig: () => {
        return config.slack;
    },
    match: (word, callback) => {
        client.on('message', (data) => {
            if (data.type === 'message') {
                if (data.text.match(new RegExp(word))) {
                    if (word === '!help') {
                        callback(false, data.channel);
                    } else {
                        callback(false)
                    }
                }
            }
        });
    },
    send: (message, channelId) => {
        if (!channelId) {
            channelId = config.slack.channel;
        }

        client.postMessage(channelId, message, {icon_url: config.slack.bot.avatar});
    }
};