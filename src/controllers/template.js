/**
 * Modules dependencies
 */


/**
 * Additional dependencies
 */

const config = require('../configs/config');

/**
 * Command controller
 */

const self = module.exports = {
    render: (name, args) => {
        return new Promise((resolve, reject) => {
            let template = config.templates[name];

            Object.keys(config.emojis).map((property, index) => {
                template = replaceAll(template, '{{ emojis.' + property + ' }}', config.emojis[property]);
            });

            Object.keys(args).map((property, index) => {
                if (property === 'status') {
                    let data = '';
                    let host = property;

                    for (let i = 0; i < args[host].length; i++) {
                        let message;

                        if (args[host][i].status.isUp) {
                            message = config.templates.isUp;
                        } else {
                            message = config.templates.isDown;
                        }

                        Object.keys(config.emojis).map((property, index) => {
                            message = replaceAll(message, '{{ emojis.' + property + ' }}', config.emojis[property]);
                        });

                        Object.keys(args[host][i]).map((property, index) => {
                            if (typeof args[host][i][property] === 'object') {
                                Object.keys(args[host][i][property]).map((subProperty, index) => {
                                    message = replaceAll(message, '{{ ' + subProperty + ' }}', args[host][i][property][subProperty]);
                                });
                            } else {
                                message = replaceAll(message, '{{ ' + property + ' }}', args[host][i][property]);
                            }
                        })

                        data += message;
                    }

                    args[property] = data;
                }

                if (property === 'host') {
                    let host = property;

                    Object.keys(args[host]).map((property, index) => {
                        if (typeof args[host][property] === 'object') {
                            Object.keys(args[host][property]).map((subProperty, index) => {
                                template = replaceAll(template, '{{ ' + subProperty + ' }}', args[host][property][subProperty]);
                            });
                        } else {
                            template = replaceAll(template, '{{ ' + property + ' }}', args[host][property]);
                        }
                    })
                }

                if (property === 'date') {
                    let date = new Date(args[property]); // TODO Fix timezone

                    args[property] = '' + ("0" + date.getDate()).slice(-2) + '/' + ("0" + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + ' ' + ("0" + (date.getHours() + 1)).slice(-2) + ':' + ("0" + (date.getMinutes() + 1)).slice(-2);
                }

                template = template.replace('{{ ' + property + ' }}', args[property]);
            });

            if (name === 'help') {
                resolve({
                    message: template,
                    args: args
                });
            } else {
                resolve(template);
            }
        });
    }
};

function replaceAll(string, search, replace) {
    search = search.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");

    return string.replace(new RegExp(search, 'g'), replace);
}