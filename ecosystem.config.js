module.exports = {
    apps : [{
        name        : "Overwatch Monitoring",
        script      : "./src/app.js",
        watch       : false,
        env: {
            "NODE_ENV": "development",
        },
        env_production : {
            "NODE_ENV": "production"
        }
    }]
};